import carte.carte;
import carte.paquet;

public class start {

	public static void main(String[] args) {

		// init
		int manche;
		int nb_partie = 1000;
		paquet pioche = new paquet();
		paquet J1 = new paquet(0);
		paquet J2 = new paquet(0);
		paquet pq_bataille = new paquet(0);
		
		

		do {

			pioche = new paquet();
			J1 = new paquet(0);
			J2 = new paquet(0);
			pq_bataille = new paquet(0);
			pioche.melanger();
			pioche.melanger();
			pioche.melanger();
			
			//System.out.println("moy : " + pioche.moyenne() );

			// distrib
			while (!pioche.vide()) {
				J1.ajouter(pioche.tirer());
				if (!pioche.vide()) {
					J2.ajouter(pioche.tirer());
				}
			}
			//System.out.println("moy J1 : " + J1.moyenne() + " - moy J2 : " + J2.moyenne());

			// jeu
			carte C1 = new carte();
			carte C2 = new carte();
			manche = 0;
			//J1.Afficher();
			J1.melanger();
			//System.out.println("");
			//J2.Afficher();
			J2.melanger();
			while (!J1.vide() && !J2.vide() && manche<100000) {
				manche++;
				if(manche%1==0)
				{
					//System.out.println(manche +" - nb carte J1 : " + J1.taille() +" - J2 : "+J2.taille()+" - B : "+pq_bataille.taille());
				}
				C1 = J1.tirer();
				C2 = J2.tirer();

				// System.out.print(manche + "- "+ C1.toString() + " contre "+ C2.toString()+"
				// => ");

				if (C1.getValeur() > C2.getValeur()) {
					J1.ajouter(C2);
					J1.ajouter(C1);
					while (!pq_bataille.vide()) {
						J1.ajouter(pq_bataille.tirer());
					}
					// System.out.print("J1");
				} else if (C2.getValeur() > C1.getValeur()) {
					J2.ajouter(C2);
					J2.ajouter(C1);
					// System.out.print("J2");
				} else {
					pq_bataille.ajouter(C1);
					pq_bataille.ajouter(C2);
					// System.out.print("Bataille");
					if (!J1.vide()) {
						pq_bataille.ajouter(J1.tirer());
						if (!J2.vide()) {
							pq_bataille.ajouter(J2.tirer());
						} else {
							//System.out.println("Joueur 2 perdu");
						}
					} else if (J2.vide()) {
						//System.out.println("Match Nul");
					} else {
						//System.out.println("Joueur 1 perdu");
					}

				}
				// System.out.println("");
			}
			if(manche!=100000)System.out.println(nb_partie + "\t" +/*"pli jou� : " + */manche);
			/*
			if (J1.vide() && J2.vide()) {
				System.out.println("Match Nul");
			} else if (J2.vide()) {
				System.out.println("Joueur 2 perdu");
			} else if (J1.vide()){
				System.out.println("Joueur 1 perdu");
			}else
			{
				System.out.println("Nb manche trop grand");
				J1.Afficher();
				System.out.println("");
				J2.Afficher();
			}
			*/
			nb_partie--;
		} while (nb_partie > 0);
		System.out.println("Fin");
	}

}
