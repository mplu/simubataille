package carte;

import java.util.ArrayList;
import java.util.Random;


public class paquet {
	private ArrayList<carte> Jeu = new ArrayList<>();
	//private Set<carte> Jeu2 = new ArrayList<>();

	public paquet() {
		Couleur couleur = Couleur.Pic;
		for (int i = 2; i <= 14; i++) {
			Jeu.add(new carte(couleur, i));
		}
		couleur = Couleur.Coeur;
		for (int i = 2; i <= 14; i++) {
			Jeu.add(new carte(couleur, i));
		}
		couleur = Couleur.Careau;
		for (int i = 2; i <= 14; i++) {
			Jeu.add(new carte(couleur, i));
		}
		couleur = Couleur.Trefle;
		for (int i = 2; i <= 14; i++) {
			Jeu.add(new carte(couleur, i));
		}

	}

	public paquet(int nb_carte) {

		//Jeu.add(new carte());

	}

	public void vider() {

		while (!Jeu.isEmpty()) {
			Jeu.remove(0);
		}

	}

	public void Afficher() {
		for (carte carte : Jeu) {
			System.out.println(carte.getValeur() + " " + carte.getCouleur());
		}
	}

	public carte tirer() {
		return Jeu.remove(0);
	}

	public void ajouter(carte newcarte) {
		Jeu.add(newcarte);
	}

	public Boolean vide() {
		return Jeu.isEmpty();
	}

	public void melanger() {
		// System.out.println("taille paquet avant melange :"+Jeu.size());
		Random rand = new Random();
		ArrayList<carte> tempJeu = new ArrayList<>();
		int max = Jeu.size();
		for (int i = 0; i < max; i++) {
			int randomIndex = rand.nextInt(Jeu.size());
			tempJeu.add(Jeu.remove(randomIndex));
		}
		Jeu = tempJeu;
		// System.out.println("taille paquet apr�s melange :"+Jeu.size());
	}
	
	public float moyenne()
	{
		float moy = 0;
		for(int i = 0;i<Jeu.size();i++)
		{
			moy = (moy * i + Jeu.get(i).getValeur())/(i+1);
		}
		return moy;
	}
	
	public int taille()
	{
		return Jeu.size();
	}

}
