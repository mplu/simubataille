package carte;

public class carte {
	private Couleur couleur;
	private Integer valeur;
	
	
	
	public Couleur getCouleur() {
		return couleur;
	}
	public void setCouleur(Couleur couleur) {
		this.couleur = couleur;
	}
	public Integer getValeur() {
		return valeur;
	}
	public void setValeur(Integer valeur) {
		this.valeur = valeur;
	}
	public carte(Couleur couleur, Integer valeur) {
		this.couleur = couleur;
		this.valeur = valeur;
	}
	public carte() {
		this.couleur = Couleur.rien;
		this.valeur = 0;
	}
	
	public carte(carte C) {
		this.couleur = C.couleur;
		this.valeur = C.valeur;
	}
	
	public String toString()
	{
		return this.getValeur() + " de " + this.getCouleur();
	}
	
	

}
